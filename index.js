class Employee {
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name(){
        return this._name;}
    set name (value){
        return this._name = value;
    }
    get age(){
        return this._age;}
    set age (value){
        return this._age = value;
    }
    get salary(){
        return this._salary;}
    set salary (value){
        return this._salary = value;
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    }
    get salary(){
        return (this._salary*3)
    }
    set salary (value){
        return this._salary = value;
    }
}

const firstProgrammer = new Programmer ('Makar', 30, 5000, 'Polish')
const secondProgrammer = new Programmer ('Karina', 23, 4000, 'English')

console.log(firstProgrammer, secondProgrammer);